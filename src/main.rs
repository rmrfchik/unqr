pub mod parser
{
    use itertools::Itertools;
    use std::io;
    use std::io::prelude::*;
#[derive(Debug)]
    enum STATE
    {
        NORMAL,
        QUOTE,
        DELIM
    }
    pub fn break_line<'a>(line: &'a str, sep: &str, quote: &str) -> Vec<String>
    {
        let mut fields = Vec::new();
        let mut start_quote = ' ';
        let mut iter = line.chars();
        let mut pos = 0;
        let mut field_start = 0;
        let mut state=STATE::NORMAL;

        let mut line_len=0;
        line.chars().for_each(|_| line_len = line_len + 1);

        while pos < line_len
        {
            //println!("pos: {:?}, len: {:?}, iter: {:?}", pos, line_len, iter);
            let c = iter.next().unwrap();
            let quote_found = quote.contains(c);
            let delim_found = sep.contains(c);
            let mut flush_field = false;
            let mut empty_field = false;
            match state 
            {
                STATE::NORMAL => 
                {
                    if quote_found 
                    {
                        start_quote = c;
                        state = STATE::QUOTE;
                        flush_field = true;
                    } else if delim_found 
                    {
                        state = STATE::DELIM;
                        flush_field = true;
                    }
                },
                STATE::QUOTE => 
                {
                    if quote_found && start_quote == c
                    {
                        state = STATE::NORMAL;
                        flush_field = true;
                        empty_field = true; // dump empty field because quotted
                    }
                },
                STATE::DELIM => 
                {
                    if quote_found 
                    {
                        state = STATE::QUOTE;
                        start_quote = c;
                        field_start = pos+1;
                    } else if delim_found
                    {
                        field_start = pos+1; // simply eat all delims
                    } else 
                    {
                        state = STATE::NORMAL;
                        field_start = pos;
                    }

                }
            }
            if flush_field
            {
                if field_start!=pos || empty_field
                {
//                    println!("line: {:?}", line);
//                    println!("start: {:?}, pos: {:?}", field_start, pos);
                    let s=line.chars().skip(field_start).take(pos-field_start).collect::<String>();
                   //println!("f: {:?}",s);
                    fields.push(s);
                }
                field_start = pos+1;
            }
            pos = pos + 1;
        }
        if field_start < pos
        {
                    let s=line.chars().skip(field_start).take(pos-field_start).collect::<String>();
//                    println!("end f: {:?}",s);
                    fields.push(s);
        }
        fields
    }

    pub fn compose_line<'a>(fields: Vec<String>, sep: &str) -> String
    {
        fields.iter().join(sep)
    }

    pub fn process(sep: &str, quote: &str, delim: &str)
    {
        let stdin = io::stdin();
        for line in stdin.lock().lines()
        {
            let line_str=&line.unwrap();
            let fields = break_line(line_str, delim, quote);
            println!("{}", compose_line(fields,sep));
        }
    }

    #[cfg(test)]
    fn do_vecs_match(a: Vec<String>, b: &Vec<&str>) -> bool {
        let matching = a.iter().zip(b.iter()).filter(|&(a, b)| a == b).count();
        matching == a.len() && matching == b.len()
    }
    #[cfg(test)]
    fn validate(input: &str, exp: &Vec<&str>)
    {
        let res = break_line(input," ","'");
        let m = do_vecs_match(res, exp);
        if !m 
        {
            println!("Mismatch {} <> {:?}", input, exp);
        }
        assert!(m);
    }
    #[cfg(test)]
    fn validate_quote(input: &str, exp: &Vec<&str>, q: &str)
    {
        let res = break_line(input," ",q);
        let resd = res.clone();
        let m = do_vecs_match(res, exp);
        if !m 
        {
            println!("Mismatch [{}] {:?} <> {:?}", input, resd, exp);
        }
        assert!(m);
    }

    #[test]
    fn test_break()
    {
        validate("this is text",&vec!["this","is","text"]);
        validate("это текст",&vec!["это","текст"]);
        validate("  this is text  ",&vec!["this","is","text"]);
        validate("this 'is' ''text",&vec!["this","is","","text"]);
        validate("this      'is'      ''  text           ",&vec!["this","is","","text"]);
        validate_quote("this      'is'      \"\"  text           ",&vec!["this","is","","text"],"\"'");
        validate_quote("this      'is'      \"\"  text           ",&vec!["this","'is'","","text"],"\"");
        validate_quote("this      'is'      \"\"  text           ",&vec!["this","is","","text"],"\"'");
        validate_quote("this      \"'is'\"      \"\"  text           ",&vec!["this","'is'","","text"],"\"'");
    }
}
extern crate clap;
use clap::{Arg, App, ArgGroup};

fn main() {
    let matches = App::new("unqr")
        .version("1.0")
        .author("paul romanchenko <paulaner@gmail.com>")
        .about("Split input lines into fields preserving quotted fields\nJoin fields with given separator")
        .arg(Arg::with_name("delimiter")
             .short("f")
             .takes_value(true)
             .help("Use given delimiter to split fields. Default space and tab"))
        .arg(Arg::with_name("single")
             .short("s")
             .long("single")
             .help("use single quote '")
             .takes_value(false))
        .arg(Arg::with_name("double")
             .short("d")
             .long("double")
             .help("use double quote \", this is default")
             .takes_value(false))
        .arg(Arg::with_name("both")
             .short("b")
             .long("both")
             .help("use both single and double quotes")
             .takes_value(false))
        .arg(Arg::with_name("quote")
             .short("q")
             .long("quote")
             .help("use given characters as quotes. Any character here will be used as opening quote and only the same character will be used as closing quote")
             .takes_value(true))
        .group(ArgGroup::with_name("quotes")
               .args(&["single", "double", "both", "quote"])
               .multiple(false))
        .arg(Arg::with_name("separator")
             .short("t")
             .long("sep")
             .takes_value(true)
             .help("Use given separator to join fields. Default |"))
        .get_matches();
    let quote=
        if matches.is_present("both")  {"\"'"} else 
            if matches.is_present("single") {"'"} else
            {matches.value_of("quote").or(Some("\"")).unwrap()};
    let delimiter = matches.value_of("delimiter").or(Some(" \t")).unwrap();
    let separator = matches.value_of("separator").or(Some("|")).unwrap();
    parser::process(separator, quote, delimiter);
}
